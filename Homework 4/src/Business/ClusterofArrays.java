/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dhruv
 */
public class ClusterofArrays {
    private ArrayList<Airliner> airlinerList;
    private ArrayList<Airplanes> airplaneList;
    private ArrayList<Flight> flightList;    
    private ArrayList<User> userList;
     
    public ClusterofArrays(){
        flightList=new ArrayList<Flight>();
        airlinerList=new ArrayList<Airliner>();
        airplaneList=new ArrayList<Airplanes>();
       
        userList=new ArrayList<User>();
        
        

}

    public ArrayList<User> getUserList() {
        return userList;
    }

    public void setUserList(ArrayList<User> userList) {
        this.userList = userList;
    }

   

    public ArrayList<Flight> getFlightList() {
        return flightList;
    }

    public void setFlightList(ArrayList<Flight> flightList) {
        this.flightList = flightList;
    }
    

    public ArrayList<Airliner> getAirlinerList() {
        return airlinerList;
    }

    public void setAirlinerList(ArrayList<Airliner> airlinerList) {
        this.airlinerList = airlinerList;
    }

    public ArrayList<Airplanes> getAirplaneList() {
        return airplaneList;
    }

    public void setAirplaneList(ArrayList<Airplanes> airplaneList) {
        this.airplaneList = airplaneList;
    }
    
     public Airliner addAirp(){
    Airliner airp=new Airliner();
    airlinerList.add(airp);
    return airp; 
   
}
     public Airplanes addAir(){
        Airplanes airadd=new Airplanes();
        airplaneList.add(airadd);
        
        return airadd; 
    
    }
     public Flight addflight(){
     Flight flt=new Flight();
     flightList.add(flt);
     return flt;
     }
     
     public User addUser()
     {
     User user=new User();
     userList.add(user);
     return user;
     }
}
