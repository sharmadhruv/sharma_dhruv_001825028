/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import javax.swing.JTextField;


/**
 *
 * @author dhruv
 */
public class Product {
    private String Productname;
    private String Productprice;
    private String Availnum;
    private String description;

    public String getProductname() {
        return Productname;
    }

    public void setProductname(String Productname) {
        this.Productname = Productname;
    }

    public String getProductprice() {
        return Productprice;
    }

    public void setProductprice(String Productprice) {
        this.Productprice = Productprice;
    }

    public String getAvailnum() {
        return Availnum;
    }

    public void setAvailnum(String Availnum) {
        this.Availnum = Availnum;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    

   

  
    
}
