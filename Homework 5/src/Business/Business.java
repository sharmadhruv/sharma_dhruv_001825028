/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dhruv
 */
public class Business {
    private PersonDirectory personDirectory;
    private UserAccountDirectory userDirectory;

    //static Business b;
    
    public Business() {
        personDirectory=new PersonDirectory();
        userDirectory=new UserAccountDirectory();
    }
    
    
//  public static synchronized Business  getInstance(){
//  if(b==null){
//  b=new Business();
//  }
//  return b;
//  }

//    public static Business getB() {
//        return b;
//    }
//
//    public static void setB(Business b) {
//        Business.b = b;
//    }
    public PersonDirectory getPersonDirectory() {
        return personDirectory;
    }

    public void setPersonDirectory(PersonDirectory personDirectory) {
        this.personDirectory = personDirectory;
    }

    public UserAccountDirectory getUserDirectory() {
        return userDirectory;
    }

    public void setUserDirectory(UserAccountDirectory userDirectory) {
        this.userDirectory = userDirectory;
    }

   
    
}
