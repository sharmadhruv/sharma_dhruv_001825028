/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dhruv
 */
public class UserAccountDirectory {
    private ArrayList<UserAccount> userAccountDirectory;

    public UserAccountDirectory() {
        userAccountDirectory=new ArrayList<UserAccount>();
    }

    public ArrayList<UserAccount> getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public void setUserAccountDirectory(ArrayList<UserAccount> userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }
    public UserAccount addUser(){
    UserAccount user=new UserAccount();
     userAccountDirectory.add(user);
     return user;
    }
    public UserAccount removeUser(UserAccount user){
    userAccountDirectory.remove(user);
    return user;
    }
    public UserAccount findUserAccount(String username){
        for(UserAccount u:userAccountDirectory){
           if(u.getUserName().equals(username))
           {
               return u;
           }
        }
        return null;
    }
    public UserAccount isValidUser(String username, String pwd, String role){
    for(UserAccount u1:userAccountDirectory){
    if(u1.getUserName().equals(username) && u1.getPassword().equals(pwd) && u1.getRole().equals(role)){
        return u1;
    }
    }
    return null;
    }
    
    
}
