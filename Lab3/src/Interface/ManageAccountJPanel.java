/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Backend.AccountDirectory;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import Backend.Account;
import Backend.AccountDirectory;
import javax.swing.JOptionPane;

/**
 *
 * @author dhruv
 */
public class ManageAccountJPanel extends javax.swing.JPanel {
private JPanel userProcessContainer;
private AccountDirectory accountDirectory;
    /**
     * Creates new form ManageAccountJPanel
     */

    ManageAccountJPanel(JPanel userProcessContainer, AccountDirectory accountDirectory) {
       initComponents();
       this.userProcessContainer=userProcessContainer;
       this.accountDirectory=accountDirectory;
       populateTable();
    }
    private void populateTable(){
        DefaultTableModel dtm=(DefaultTableModel) tblAccounts.getModel();
        dtm.setRowCount(0);
        for(Account account: accountDirectory.getAccountList()){
            Object[] row= new Object[4];
            row[0]=account;
            row[1]=account.getRoutingNumber();
            row[2]=account.getAccountNumber();
            row[3]=account.getBalance();
            
            dtm.addRow(row);
        
        }
              
    
    
    
    
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblAccounts = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();
        txtAccountNumber = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();

        jButton1.setText("Delete Account");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        tblAccounts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Bank Name", "Routing Number", "Account Number", "Balance"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblAccounts);

        jButton2.setText("Search");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("View Details");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        btnBack.setText("<<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(161, 161, 161)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBack)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(47, 47, 47)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1)
                            .addComponent(txtAccountNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(84, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(txtAccountNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(98, 98, 98)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 238, Short.MAX_VALUE)
                .addComponent(btnBack)
                .addGap(56, 56, 56))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
    userProcessContainer.remove(this);
    CardLayout layout=(CardLayout) userProcessContainer.getLayout();
    layout.previous(userProcessContainer);
        
// TODO add your handling code here:
    }//GEN-LAST:event_btnBackActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    int selectedRow=tblAccounts.getSelectedRow();
    if(selectedRow >=0){
        int dialoguebtn=JOptionPane.YES_NO_OPTION;
        int dialogueResult=JOptionPane.showConfirmDialog(null, "Would you like to delete the selected account?", "Warning", JOptionPane.WARNING_MESSAGE, dialoguebtn);
        if(dialogueResult==JOptionPane.YES_OPTION)
        {
            Account account=(Account) tblAccounts.getValueAt(selectedRow, 0);
            accountDirectory.deleteAccount(account);
            populateTable();
        }
        
    
    
    }
    else{
        JOptionPane.showMessageDialog(null, "Please select a Row!", "Warning", JOptionPane.WARNING_MESSAGE);
    }


        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
    int selectedRow=tblAccounts.getSelectedRow();
    if(selectedRow <0)
    {
        JOptionPane.showMessageDialog(null, "Please select a row to view details!!","Warning", JOptionPane.WARNING_MESSAGE); 
    
    }
    else{
        Account account=(Account) tblAccounts.getValueAt(selectedRow, 0);
        ViewAccountJPanel panel= new ViewAccountJPanel(userProcessContainer, account);
        userProcessContainer.add("ViewAccountJPanel", panel);
        CardLayout layout= (CardLayout) userProcessContainer.getLayout(); 
        layout.next(userProcessContainer);
        
    
    }


        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
    Account result= accountDirectory.searchAccount(txtAccountNumber.getText());
    if(result== null){
        JOptionPane.showMessageDialog(null, "Account doesn't exists!!","Information", JOptionPane.INFORMATION_MESSAGE);
       
    }
    else{
       
        ViewAccountJPanel panel= new ViewAccountJPanel(userProcessContainer, result);
        userProcessContainer.add("ViewAccountJPanel", panel);
        CardLayout layout= (CardLayout) userProcessContainer.getLayout(); 
        layout.next(userProcessContainer);
    
    
    
    
    
    }


        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblAccounts;
    private javax.swing.JTextField txtAccountNumber;
    // End of variables declaration//GEN-END:variables
}
