
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dhruv
 */
public class ClusterOfArrays {
    private ArrayList<Airliner> loa;
    private ArrayList<Airplane> ap;
    private ArrayList<Seat> st;

    public ArrayList<Airliner> getLoa() {
        return loa;
    }

    public void setLoa(ArrayList<Airliner> loa) {
        this.loa = loa;
    }

    public ArrayList<Airplane> getAp() {
        return ap;
    }

    public void setAp(ArrayList<Airplane> ap) {
        this.ap = ap;
    }

    public ArrayList<Seat> getSt() {
        return st;
    }

    public void setSt(ArrayList<Seat> st) {
        this.st = st;
    }
    public ClusterOfArrays() {
         loa = new ArrayList<Airliner>();
         ap = new ArrayList<Airplane>();
         st = new ArrayList<Seat>();
        
        
    }
    
    
    public Airliner addAirp(){
    Airliner airp=new Airliner();
    loa.add(airp);
    return airp;   
    
    
    
    }
    public Airplane addAir(){
        Airplane airadd=new Airplane();
        ap.add(airadd);
        
        return airadd; 
    
    }
    
     public Seat addSeat(){
     Seat sa=new Seat();
     st.add(sa);
     return sa;   
     
     
     }    
    
    
    
    

}
