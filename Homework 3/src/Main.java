
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.xml.bind.DatatypeConverter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dhruv
 */
public class Main {
   
    
    
    
    public static void main(String[] args) {
       String csvFile = "MasterScheduler.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        ClusterOfArrays loa=new ClusterOfArrays();
        ClusterOfArrays ap=new ClusterOfArrays();
        ClusterOfArrays st=new ClusterOfArrays();
        
       
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] masterElement = line.split(cvsSplitBy);
                Airliner ar= loa.addAirp();
                Airplane an=ap.addAir();
                Seat se=st.addSeat();
                String airline= masterElement[0];
                String modelno=masterElement[1];
                String w1=masterElement[2];
                String m1=masterElement[3];
                String a1=masterElement[4];
                String w2=masterElement[5];
                String m2=masterElement[6];
                String a2=masterElement[7];
                
                ar.setCompany(airline);
                an.setModelno(modelno);
                se.setA1(a1);
                se.setA2(a2);
                se.setM1(m1);
                se.setM2(m2);
                se.setW1(w1);
                se.setW2(w2);
            }
            
                
                
               
            
            int i=1;
            for (Airliner a : loa.getLoa()) {
                System.out.println(i+":Airliner-->"+a.getCompany());
                i++;    }      
            for (Airplane b : ap.getAp() ){
            System.out.println("\t:Airplane-->"+b.getModelno());
            }
            for(Seat c : st.getSt()){
                
                System.out.println("\tLeft Window Seat-->>"+c.getW1()+ "\tLeft Middle Seat-->>"+c.getM1() +"\tLeft Aisle Seat-->>"+c.getA1()+ "\tRight Window Seat-->>"+c.getW2()+ "\tRight Middle Seat-->"+c.getM2()+ "\tRight Aisle Seat-->"+c.getA2());
            }
            
                
            Revenue (loa,ap,st);
          

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    public static void Revenue(ClusterOfArrays a,ClusterOfArrays b, ClusterOfArrays c){
        int revenue1=0;
        int revenue2=0;
        int revenue3=0;
        int revenue4=0;
        for(Airliner airln : a.getLoa()){
            
            for(Airplane airpln: b.getAp()){
                
                for(Seat seat:c.getSt()){
                    if(airln.getCompany().equalsIgnoreCase("Lufthansa")){
                    
                    if(airpln.getModelno().equalsIgnoreCase("11211"))
                    {
                    if(seat.getW1().equals("A1"))
                        revenue1=revenue1+150;    
                    }
                    else if(seat.getM1().equals("B9")){
                    revenue1=revenue1+135;
                    }
                    else if(seat.getA1().equals("C2")){
                    revenue1=revenue1+110;
                    }
                    else if(seat.getW2().equals("D3")){
                    revenue1=revenue1+150;
                    }
                    else if(seat.getM2().equals("E6")){
                    revenue1=revenue1+135;
                    }
                    else if(seat.getA2().equals("F2"))
                    {revenue1=revenue1+110;
                    }
                    
                    }
                      if(airpln.getModelno().equalsIgnoreCase("11212"))
                    {
                    if(seat.getW1().equals("A1"))
                        revenue2=revenue2+150;    
                    }
                    else if(seat.getM1().equals("B9")){
                    revenue2=revenue2+135;
                    }
                    else if(seat.getA1().equals("C2")){
                    revenue2=revenue2+110;
                    }
                    else if(seat.getW2().equals("D3")){
                    revenue2=revenue2+150;
                    }
                    else if(seat.getM2().equals("E6")){
                    revenue2=revenue2+135;
                    }
                    else if(seat.getA2().equals("F2"))
                    {revenue2=revenue2+110;
                    }
                    
                    
                    
                
                
                
                
         if(airln.getCompany().equalsIgnoreCase("Swiss")){
         if(airpln.getModelno().equalsIgnoreCase("90914")){
         if(seat.getW1().equals("A1"))
                        revenue3=revenue3+200;    
                    }
                    else if(seat.getM1().equals("B9")){
                    revenue3=revenue3+170;
                    }
                    else if(seat.getA1().equals("C2")){
                    revenue3=revenue3+150;
                    }
                    else if(seat.getW2().equals("D3")){
                    revenue3=revenue3+200;
                    }
                    else if(seat.getM2().equals("E6")){
                    revenue3=revenue3+170;
                    }
                    else if(seat.getA2().equals("F2"))
                    {revenue3=revenue3+150;
                    }
         }
         if(airpln.getModelno().equalsIgnoreCase("90915")){
         if(seat.getW1().equals("A1"))
                        revenue4=revenue4+200;    
                    }
                    else if(seat.getM1().equals("B9")){
                    revenue4=revenue4+170;
                    }
                    else if(seat.getA1().equals("C2")){
                    revenue4=revenue4+150;
                    }
                    else if(seat.getW2().equals("D3")){
                    revenue4=revenue4+200;
                    }
                    else if(seat.getM2().equals("E6")){
                    revenue4=revenue4+170;
                    }
                    else if(seat.getA2().equals("F2"))
                    {revenue4=revenue4+150;
                    }
         
         }
        
        }
        }
        
    
    
    
       System.out.println("\nRevenue of Lufthansa, Flight No. 11211 is -->" +revenue1);
       System.out.println("\nRevenue of Lufthansa, Flight No. 11212 is -->" +revenue2);
       System.out.println("\nRevenue of Swiss, Flight No. 8084 is -->" +revenue3);
       System.out.println("\nRevenue of Swiss, Flight No. 8085 is -->" +revenue4);
       
       int revenue5= revenue1+revenue2;
       int revenue6=revenue3+revenue4;
       int revenue7=revenue5+revenue6;
        System.out.println("\nRevenue of Lufthansa is -->" +revenue5);
        System.out.println("\nRevenue of Lufthansa is -->" +revenue6);
        
        
        
         System.out.println("\nRevenue of BostonGo is -->" +revenue7);
       
    }
        
    }


