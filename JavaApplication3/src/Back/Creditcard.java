/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Back;

/**
 *
 * @author dhruv
 */
public class Creditcard {
    public String credno;
    public String credthru;
    public String credcode;
    public String credbank;

    public String getCredno() {
        return credno;
    }

    public void setCredno(String credno) {
        this.credno = credno;
    }

    public String getCredthru() {
        return credthru;
    }

    public void setCredthru(String credthru) {
        this.credthru = credthru;
    }

    public String getCredcode() {
        return credcode;
    }

    public void setCredcode(String credcode) {
        this.credcode = credcode;
    }

    public String getCredbank() {
        return credbank;
    }

    public void setCredbank(String credbank) {
        this.credbank = credbank;
    }
}
