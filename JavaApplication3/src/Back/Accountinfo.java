/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Back;

/**
 *
 * @author dhruv
 */
public class Accountinfo {
    public String checkaccno;
    public String checkdate;
    public String checkdebt;
    public String checkcred;
    public String saveaccno;
    public String savedate;
    public String savedebt;
    public String savecred;

    public String getCheckaccno() {
        return checkaccno;
    }

    public void setCheckaccno(String checkaccno) {
        this.checkaccno = checkaccno;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }

    public String getCheckdebt() {
        return checkdebt;
    }

    public void setCheckdebt(String checkdebt) {
        this.checkdebt = checkdebt;
    }

    public String getCheckcred() {
        return checkcred;
    }

    public void setCheckcred(String checkcred) {
        this.checkcred = checkcred;
    }

    public String getSaveaccno() {
        return saveaccno;
    }

    public void setSaveaccno(String saveaccno) {
        this.saveaccno = saveaccno;
    }

    public String getSavedate() {
        return savedate;
    }

    public void setSavedate(String savedate) {
        this.savedate = savedate;
    }

    public String getSavedebt() {
        return savedebt;
    }

    public void setSavedebt(String savedebt) {
        this.savedebt = savedebt;
    }

    public String getSavecred() {
        return savecred;
    }

    public void setSavecred(String savecred) {
        this.savecred = savecred;
    }
    
    
}
