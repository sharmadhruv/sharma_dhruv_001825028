/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Back;

/**
 *
 * @author dhruv
 */
public class License {
    public String licenseno;
    public String licensethru;
    public String licensedate;
    public String licensecountry;
    public String licensetype;

    public String getLicenseno() {
        return licenseno;
    }

    public void setLicenseno(String licenseno) {
        this.licenseno = licenseno;
    }

    public String getLicensethru() {
        return licensethru;
    }

    public void setLicensethru(String licensethru) {
        this.licensethru = licensethru;
    }

    public String getLicensedate() {
        return licensedate;
    }

    public void setLicensedate(String licensedate) {
        this.licensedate = licensedate;
    }

    public String getLicensecountry() {
        return licensecountry;
    }

    public void setLicensecountry(String licensecountry) {
        this.licensecountry = licensecountry;
    }

    public String getLicensetype() {
        return licensetype;
    }

    public void setLicensetype(String licensetype) {
        this.licensetype = licensetype;
    }
    
}
